const express = require('express')
const app = express()
const cors = require('cors')
const PORT = 8000

app.use(cors())

const rideDetails = {
    'default':  {
        'organizer': 'Organizer Name',
        'rideName': 'Ride Name',
        'rideStartTime': 'Start Time'
    },
    'pure water ride': {
        'organizer': 'ActiveSGV & Pure Water Southern California',
        'rideName': 'Pure Water Ride',
        'rideStartTime': '8:00 AM'
    },
    'south el monte community ride': {
        'organizer': 'South El Monte',
        'rideName': 'South El Monte  Community Ride',
        'rideStartTime': '8:30 AM'
    },
    'unknown': {
        'organizer': 'N/A',
        'rideName': 'N/A',
        'rideStartTime': 'N/A'
    }
}

// requests to main route
app.get('/', (req, res)=>{
    res.sendFile(__dirname + '/index.html')
})

// requests to /api endpoint
app.get('/api/:ride', (req, res)=>{
    const bikeRide = req.params.ride.toLowerCase()
    if (rideDetails[bikeRide]) {
        res.json(rideDetails[bikeRide])
    }   else {
        res.json(rideDetails['unknown'])
    }
})

app.listen(PORT, ()=>{
    console.log(`The server is  now running on port ${PORT}`)
})
